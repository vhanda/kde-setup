#!/usr/bin/python
# -*- coding: utf-8 -*-

import psutil

def inMB( item ) :
  return (float(item) / 1024.0 / 1024.0)
  
def getProcesses() :
    plist = []
    pidlist = psutil.get_pid_list()
    for pid in pidlist :
        try :
            p = psutil.Process( pid )
            name = ""
            if "nepomukserver" in p.name :
                name = "nepomukserver"
            elif "nepomukservicestub" in p.name :
                name = p.cmdline[-1] # The last member which is the name of the service
            elif "virtuoso" in p.name :
                name = "virtuoso"
            elif "nepomukindexer" in p.name :
                name = "nepomukindexer"

            if name :
                rss, vms =  p.get_memory_info()
                rss = inMB( rss )
                process = Process()
                process.name = name
                process.pid = p.pid
                process.mem = rss
                process.mem_per = p.get_memory_percent()
                process.cpu_per = p.get_cpu_percent()
                
                plist.append( process )
        except psutil.error.NoSuchProcess :
            continue
            
    return plist
    
        
class Process :
    name = ""
    pid = ""
    mem = ""
    mem_per = ""
    cpu_per = ""
    
    
if __name__ == '__main__':
    processList = getProcesses()
    totalCpu = 0
    totalMem = 0
    totalMemPer = 0
    for p in processList :
        print( p.pid, ("%.2f" % p.cpu_per) + '%', ( "%.2f" % p.mem ) + "Mb", "( " + "%.2f" % p.mem_per + "% )", p.name )
        totalCpu += p.cpu_per
        totalMem += p.mem
        totalMemPer += p.mem_per

    print( "\nTotal Cpu:", ("%.2f" % totalCpu) +'%' )
    print( "Total Mem:", ("%.2f" % totalMem) + "Mb", "( " +  ("%.2f" % totalMemPer) +"% )" )
 
